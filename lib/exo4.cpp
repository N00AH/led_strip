//---------------------------------------------------------------------------
// Fichier "exo4.cpp"
// Application console.
//chenillard unidirectionnel avec 3 LEDS actives
//Exo4
//Nemri Noah 17/03/2021
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>
#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban

int i = 0;
int cpt = 1;      //compteur pour avancer et reculer sur le ruban led
int cptColor = 0; //compteur pour changer de couleur
int Hue = 0;
int pos = 0;  //Position des LEDS
int sens = 1; // Sens des leds
//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}
void loop()
{
    // commander les LEDS
    FastLED.clear();
    FastLED.show();
    leds[pos] = CHSV(160, 200, 40);             //LED GAUCHE
    leds[(pos + 1) % 10] = CHSV(160, 255, 255); //LED PRINCIPALE
    leds[(pos + 2) % 10] = CHSV(160, 200, 40);  //LED DROITE
    pos = pos + sens;
    FastLED.show();
    delay(100);
    if (pos >= 10)
    {
        pos = 0;
    }
}