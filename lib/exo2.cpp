//---------------------------------------------------------------------------
// Fichier "exo2.cpp"
// Application console.
//Chenillard bidirectionnel
//Exo2
//Nemri Noah 15/03/2021
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>
#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban

int i = 0;
int cpt = 1;
//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}
void loop()
{
    leds[i] = CRGB::Green;
    FastLED.show();
    delay(250);
    leds[i] = CRGB::Black;
    FastLED.show();
    i = i + cpt;

    if (i >= NUM_LEDS - 1)
    {
        cpt = -1;
    }
    else if (i <= 0)
    {
        cpt = 1;
    }
}
