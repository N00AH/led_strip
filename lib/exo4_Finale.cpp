//---------------------------------------------------------------------------
// Fichier "exo4_Finale.cpp"
// Application console.
//chenillard unidirectionnel avec 3 LEDS actives
//Exo4
//Nemri Noah 17/03/2021
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>
#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban
//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
int LedPrincipal = 0;
int sens = 1;
int i = 0;
int cpt = 1;      //compteur pour avancer et reculer sur le ruban led
int cptColor = 0; //compteur pour changer de couleur
int Hue = 0;
int pos = 0;  //Position des LEDS

void allumerLeds(int prmLedPrincipal);
void setup()
{

    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}
void allumerLeds(int prmLedPrincipal)
{
    FastLED.clear();
    leds[prmLedPrincipal] = CHSV(160, 200, 40);
    leds[(prmLedPrincipal + 1) % 10] = CHSV(160, 255, 255);
    leds[(prmLedPrincipal + 2) % 10] = CHSV(160, 200, 50);
    FastLED.show();
}
void loop()
{

    allumerLeds(LedPrincipal);
    LedPrincipal++;

    if (LedPrincipal >= NUM_LEDS)
    {
        LedPrincipal = 0;
    }
    delay(200);
}